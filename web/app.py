from flask import Flask, render_template, request
app = Flask(__name__)

@app.route("/<path:name>")
def page(name):
    check403 = request.environ['REQUEST_URI']
    if ('~' in check403) or ('//' in check403)  or ('..' in check403) :
        return render_template('403.html'), 403
    else:
        try:
            return render_template(name)
        except:
            return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
